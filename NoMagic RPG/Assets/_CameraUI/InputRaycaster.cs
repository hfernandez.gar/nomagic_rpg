﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Characters.NPC;
using RPG.Characters.Enemy;
using UnityEngine.EventSystems;
using RPG.Characters.Player;
using VIDE_Data;
using RPG.Game;
using System;

namespace RPG.Cameras
{
    public class InputRaycaster : MonoBehaviour
    {

        private const int ENEMY_LAYER = 9;
        private const int NPC_LAYER = 10;
        private const int DESTROYABLE_LAYER = 12;

        [SerializeField]
        Texture2D normalCursor = null;
        [SerializeField]
        Texture2D hintCursor = null;
        [SerializeField]
        Texture2D targetCursor = null;
        [SerializeField]
        Vector2 cursorHotspot = Vector2.zero;

        float maxRaycastDepth = 100f; // Hard coded value
        PlayerMovement playerMovement;


        // Setup delegates for broadcasting layer changes to other classes
        public delegate void OnInputEnemy(EnemyAI enemy);
        public event OnInputEnemy notifyEnemyObservers;

        public delegate void OnInputNPC(NPC npc);
        public event OnInputNPC notifyNPCObservers;

        public delegate void OnInputDestroyable(DestroyThingByPlayer destroyable);
        public event OnInputDestroyable notifyDestroyableObservers;

        // Use this for initialization
        void Start()
        {
            playerMovement = FindObjectOfType<PlayerMovement>();
        }

        // Update is called once per frame
        void Update()
        {

            if (!PlayerMovement.isInJoystickMode)
            {
                if (VD.isActive || Camera.main == null)
                {
                    Cursor.SetCursor(normalCursor, cursorHotspot, CursorMode.Auto);
                    return;
                }
                maxRaycastDepth = 100f;
                // Check if pointer is over an interactable UI element
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    //Implement UI interaction
                }
                else
                {
                    PerformRaycasts();
                }
            }
            else
            {
                if(VD.isActive || Camera.main == null)  { return; }
                maxRaycastDepth = playerMovement.GetInteractRadius();
                PerformRaycasts();
            }
        }


        private void PerformRaycasts()
        {
            Ray ray;

            if(PlayerMovement.isInJoystickMode)
                ray = new Ray(PlayerManager.instance.playerTransform.position, PlayerManager.instance.playerTransform.forward);
            else
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            //Specify layer priorities below, order matters
            if (RaycastForEnemy(ray)) { return; }
            if (RaycastForDestroyable(ray)) { return; }
            if (RaycastForNPC(ray)) { return; }
            Cursor.SetCursor(normalCursor, cursorHotspot, CursorMode.Auto);

        }

        private bool RaycastForNPC(Ray ray)
        {
            RaycastHit hitInfo;
            int layerMask = (1 << NPC_LAYER);
            Physics.Raycast(ray, out hitInfo, maxRaycastDepth, layerMask);

            if (hitInfo.transform != null)
            {
                var npc = hitInfo.collider.gameObject.GetComponent<NPC>();
                if (npc)
                {
                    Cursor.SetCursor(hintCursor, cursorHotspot, CursorMode.Auto);
                    OnInputOverNPC(npc);
                    return true;
                }
            }
            return false;
        }

        private bool RaycastForDestroyable(Ray ray)
        {
            RaycastHit hitInfo;
            int layerMask = (1 << DESTROYABLE_LAYER);
            Physics.Raycast(ray, out hitInfo, maxRaycastDepth, layerMask);

            if (hitInfo.transform != null)
            {
                var destroyable = hitInfo.collider.gameObject.GetComponent<DestroyThingByPlayer>();
                if (destroyable)
                {
                    Cursor.SetCursor(targetCursor, cursorHotspot, CursorMode.Auto);
                    OnInputOverDestroyable(destroyable);
                    return true;
                }
            }
            return false;
        }

        

        private bool RaycastForEnemy(Ray ray)
        {
            RaycastHit hitInfo;

            int layerMask = (1 << ENEMY_LAYER);
            Physics.Raycast(ray, out hitInfo, maxRaycastDepth, layerMask);
            if (hitInfo.transform != null)
            {

                GameObject enemyGameObject = hitInfo.collider.gameObject;
                EnemyAI enemyHit = enemyGameObject.GetComponent<EnemyAI>();
                if (!enemyHit) //If it's empty is because it collided with a quad
                    enemyHit = enemyGameObject.GetComponentInParent<EnemyAI>();

                if (enemyHit)
                {
                    Cursor.SetCursor(targetCursor, cursorHotspot, CursorMode.Auto);
                    onInputOverEnemy(enemyHit);
                    return true;
                }

            }
            return false;
        }

        private void onInputOverEnemy(EnemyAI enemyHit)
        {
            notifyEnemyObservers(enemyHit.gameObject.GetComponent<EnemyAI>());
        }

        private void OnInputOverNPC(NPC npc)
        {
            notifyNPCObservers(npc);
        }

        private void OnInputOverDestroyable(DestroyThingByPlayer destroyable)
        {
            notifyDestroyableObservers(destroyable);
        }


    }

}