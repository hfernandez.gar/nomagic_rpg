﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Characters.Player
{
    public class SpecialAbilityUI : MonoBehaviour
    {
        Image ui = null;
        Player player = null;



        // Use this for initialization
        void Start()
        {
            player = FindObjectOfType<Player>();
            ui = GetComponent<Image>();
        }

        // Update is called once per frame
        void Update()
        {
            ui.sprite = player.GetSpritesFromSpecialAbilities()[player.GetAbilityIndex()];
        }
    }
}