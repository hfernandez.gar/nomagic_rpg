﻿using RPG.Game;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VIDE_Data;

namespace RPG.UI
{

    public class UIManager : MonoBehaviour
    {

        public GameObject dialogueContainer;
        public GameObject playerChoicesContainer;
        public TextMeshProUGUI dialogueText;
        public TextMeshProUGUI[] textPlayerChoices;

        [Header("Options")]
        public bool isNPCDialogueAnimated;
        public float NPC_secsPerLetter;

        [HideInInspector]
        public static int playerOptionsLength;


        IEnumerator TextAnimator;
        bool animatingText = false; //Will help us know when text is currently being animated
        bool choiceDialogueButton = false;

        public static UIManager instance;

        void MakeSigleton()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        void Awake()
        {
            MakeSigleton();
        }

        // Use this for initialization
        void Start()
        {
            dialogueContainer.SetActive(false);
            playerChoicesContainer.SetActive(false);
            playerOptionsLength = textPlayerChoices.Length - 1;
            
        }

        // Update is called once per frame
        void Update()
        {
            //Lets just store the Node Data variable for the sake of fewer words
            //var data = VD.nodeData;


        }

        //This begins the dialogue and progresses through it
        public void Interact(VIDE_Assign dialogue)
        {
            if (!VD.isActive)
            {
                Begin(dialogue);
            }
            else
            {
                if (VD.nodeData.pausedAction) {
                    dialogueContainer.SetActive(false);
                    playerChoicesContainer.SetActive(false);
                    return;
                }
                CallNext();
            }
        }

        private void Begin(VIDE_Assign dialogue)
        {
            dialogueContainer.SetActive(true);
            VD.BeginDialogue(dialogue);

            VD.OnNodeChange += UpdateUI;
            VD.OnActionNode += ActionNode;
            VD.OnEnd += End;

            if (isNPCDialogueAnimated)
            {
                //This coroutine animates the NPC text instead of displaying it all at once
                TextAnimator = AnimateNPCText(VD.nodeData.comments[VD.nodeData.commentIndex]);
                StartCoroutine(TextAnimator);
            }
            else
            {
                dialogueText.text = VD.nodeData.comments[VD.nodeData.commentIndex];
            }


        }

        private void End(VD.NodeData data)
        {
            dialogueContainer.SetActive(false);
            playerChoicesContainer.SetActive(false);
            VD.OnNodeChange -= UpdateUI;
            VD.OnActionNode -= ActionNode;
            VD.OnEnd -= End;
            VD.EndDialogue();
        }

        private void ActionNode(int id)
        {
            if (id == 4)
            {
                dialogueContainer.SetActive(false);
                playerChoicesContainer.SetActive(false);
            }
        }

        private void UpdateUI(VD.NodeData data)
        {
            dialogueContainer.SetActive(false);
            playerChoicesContainer.SetActive(false);

            if (data.pausedAction) { return; }

            if (data.isPlayer)
            {
                playerChoicesContainer.SetActive(true);

                for (int i = 0; i < textPlayerChoices.Length; i++)
                {
                    if (i < data.comments.Length)
                    {
                        textPlayerChoices[i].transform.parent.gameObject.SetActive(true);
                        textPlayerChoices[i].text = data.comments[i];
                    }
                    else
                    {
                        textPlayerChoices[i].transform.parent.gameObject.SetActive(false);
                    }
                }

            }
            else
            {
                dialogueContainer.SetActive(true);

                if (isNPCDialogueAnimated)
                {
                    //This coroutine animates the NPC text instead of displaying it all at once
                    TextAnimator = AnimateNPCText(data.comments[data.commentIndex]);
                    StartCoroutine(TextAnimator);
                }
                else
                {
                    dialogueText.text = data.comments[data.commentIndex];
                }

            }
        }

        public void SetPlayerChoice(int choice)
        {
            choiceDialogueButton = true;
            VD.nodeData.commentIndex = choice;
            VD.Next();
        }

        void OnDisable()
        {
            if (dialogueContainer != null)
            {
                End(null);
            }
        }

        public void SetNode(int id)
        {
            VD.SetNode(id);
        }

        //Calls next node in the dialogue
        public void CallNext()
        {
            //Let's not go forward if text is currently being animated, but let's speed it up.
            if (animatingText) { CutTextAnim(); return; }

            //Check if the player has choices & that it hasn't selected any option
            if (VD.nodeData.isPlayer && !choiceDialogueButton)
            {
                return;
            }
            else if (!VD.nodeData.isPlayer)
            {
                choiceDialogueButton = false;
            }
            VD.Next(); //We call the next node and populate nodeData with new data. Will fire OnNodeChange.

        }

        void CutTextAnim()
        {
            StopCoroutine(TextAnimator);
            if (VD.nodeData.isPlayer)
            {
                /*availableChoices = 0;
                for (int i = 0; i < VD.nodeData.comments.Length; i++)
                {
                    maxPlayerChoices[i].transform.GetChild(0).GetComponent<Text>().text = VD.nodeData.comments[i]; //Assumes first child of button gameobject is text gameobject
                    maxPlayerChoices[i].gameObject.SetActive(true);
                    availableChoices++;
                }
                if (useNavigation)
                    maxPlayerChoices[0].Select();*/
            }
            else
            {
                dialogueText.text = VD.nodeData.comments[VD.nodeData.commentIndex]; //Now just copy full text	
            }
            animatingText = false;
        }

        IEnumerator AnimateNPCText(string text)
        {
            animatingText = true;
            dialogueText.text = "";

            string[] words = text.Split(' ');

            for (int i = 0; i < words.Length; i++)
            {
                string word = words[i];
                if (i != words.Length - 1) word += " ";

                string previousText = dialogueText.text;

                float lastHeight = dialogueText.preferredHeight;
                dialogueText.text += word;
                if (dialogueText.preferredHeight > lastHeight)
                {
                    previousText += System.Environment.NewLine;
                }

                for (int j = 0; j < word.Length; j++)
                {
                    dialogueText.text = previousText + word.Substring(0, j + 1);
                    yield return StartCoroutine(TimeUtils.WaitForRealSeconds(NPC_secsPerLetter)); //new WaitForSeconds(NPC_secsPerLetter);
                }
            }
            dialogueText.text = text;
            animatingText = false;
        }

    }
}