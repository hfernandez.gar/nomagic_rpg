﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RPG.Game;
using System;
using TMPro;

namespace RPG.Menu
{

    public class WeaponsMenu : MonoBehaviour
    {

        [SerializeField]
        GameObject weaponMenu;

        [SerializeField]
        Button buttonPrefab;

        List<Button> buttons;

        // Use this for initialization
        void Start()
        {

            buttons = new List<Button>();
            foreach(var weapon in GameManager.instance.weapons)
            {
                var button = Instantiate(buttonPrefab, weaponMenu.transform);
                button.GetComponentInChildren<TextMeshProUGUI>().text = weapon.Value.name;

                button.onClick.AddListener(delegate { OnClickWeaponButton(weapon.Key, button); });

                SelectionedWeaponButton(weapon.Key, button);


                buttons.Add(button);
                
            }


            GameManager.instance.notifyPlayerWeaponGetObservers += OnPlayerWeaponGet;

        }

        private void OnPlayerWeaponGet(int weaponIndex)
        {
            var button = Instantiate(buttonPrefab, weaponMenu.transform);
            var weapon = GameManager.instance.weapons[weaponIndex];
            button.GetComponentInChildren<TextMeshProUGUI>().text = weapon.name;

            button.onClick.AddListener(delegate { OnClickWeaponButton(weaponIndex, button); });

            SelectionedWeaponButton(weaponIndex, button);

            buttons.Add(button);

            MakeOtherButtonsInteractable(button);

        }

        private void MakeOtherButtonsInteractable(Button button)
        {
            foreach(var b in buttons)
            {
                if (!b.GetComponentInChildren<TextMeshProUGUI>().text.Equals(button.GetComponentInChildren<TextMeshProUGUI>().text))
                    b.interactable = true;

            }
        }

        private static void SelectionedWeaponButton(int weaponIndex, Button button)
        {
            if (GameManager.instance.selectedWeapon == weaponIndex)
            {
                button.interactable = false;
            }
        }

        void OnClickWeaponButton(int weaponIndex, Button button)
        {
            if (GameManager.instance.selectedWeapon != weaponIndex)
            {
                GameManager.instance.selectedWeapon = weaponIndex;
                button.interactable = false;
                GameManager.instance.OnPlayerWeaponOnChange(GameManager.instance.selectedWeapon);

                MakeOtherButtonsInteractable(button);
            }
        }

        void RemoveAllListeners()
        {
            buttons.ForEach(b => b.onClick.RemoveAllListeners());
        }

        // Update is called once per frame
        void Update()
        {
           
        }
    }
}
