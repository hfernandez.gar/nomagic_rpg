using RPG.Menu;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {

	WeaponsMenu weaponsMenu;

	// Use this for initialization
	void Start () {
		weaponsMenu = FindObjectOfType<WeaponsMenu>();
	}


	public void OnButtonWeaponsMenu()
	{
		gameObject.GetComponent<CanvasGroup>().alpha = 0.0f;
		gameObject.GetComponent<CanvasGroup>().interactable = false;
		gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;

		weaponsMenu.gameObject.GetComponent<CanvasGroup>().alpha = 1.0f;
		weaponsMenu.gameObject.GetComponent<CanvasGroup>().interactable = true;
		weaponsMenu.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;

	}

	public void OnButtonQuitMenu()
    {
		Application.Quit();
    }

}
