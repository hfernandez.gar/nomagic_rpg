﻿using UnityEngine;

namespace RPG.Utilities
{

    public class FacingBillboard : MonoBehaviour
    {
        public Transform gameObjectTransform;

        void Update()
        {

            transform.LookAt(transform.position + gameObjectTransform.rotation * Vector3.forward,
                gameObjectTransform.rotation * Vector3.up);
        }
    }
}