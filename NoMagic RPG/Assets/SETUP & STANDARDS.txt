# Setup and Standards Doc
The purpose of this short doc is to get us all on the same page regarding a few important things such as world scale, layers, prefab setups, etc.

## World Scale & Origin
We will be using Unity's default 1 m = 1 Unity Unit. Note animations don't scale well. This prototype level is on a plane with a scale of 200, 100, 200 & the player character  

## Layers
So far the layers of note are `Walkable` and `Enemy`, and are self-explanatory in their use. Note it is the collider that receives the raycast, so do apply layers to children.
-Going to change the names of the layer beacuse of the third person camera.

##NPC
There is one NPC that is going to give a sidequest to the player (beating multiple enemies in the "city hall" & get a flame weapon)
and another NPC that if the player got something he could burn some bushes for the player.

##Camera
FOV is 40

## Ramp angles
Maximum ramp Z value = 15
For a longer ramp = 10

##Textures
Size for particles: 512 x 512 pixels
