﻿using RPG.Cameras;
using RPG.Characters.Enemy;
using RPG.Characters.Player;
using RPG.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Game {

    public class TriggerBossThings : MonoBehaviour {

        public UIManager diagUI;
        Boss boss;
        AudioSource musicPlayer;
        [SerializeField]
        AudioClip bossMusic;


        // Use this for initialization
        void Start() {
            boss = FindObjectOfType<Boss>();
            musicPlayer = FindObjectOfType<Music>().GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update() {
            if (boss.GetComponent<VIDE_Assign>().interactionCount >= 1)
            {
                TimeUtils.timeScaleToggle();
                ToggleCameraTarget(FindObjectOfType<Player>().transform, false);

                musicPlayer.Stop();
                musicPlayer.clip = bossMusic;
                musicPlayer.Play();

                Destroy(gameObject);
            }
        }

        private void ToggleCameraTarget(Transform target, bool isCameraRotationLocked)
        {
            FindObjectOfType<FreeLookCam>().SetTarget(target);
            FindObjectOfType<FreeLookCam>().SetLockCameraRotation(isCameraRotationLocked);
        }


        void OnTriggerEnter(Collider other)
        {
            if (other.tag.Equals("Player"))
            {

                diagUI.Interact(boss.GetComponent<VIDE_Assign>());
                FindObjectOfType<FreeLookCam>().GetComponentInChildren<Camera>().enabled = false;
                FindObjectOfType<BossCamera>().GetComponentInChildren<Camera>().enabled = true;
                ToggleCameraTarget(FindObjectOfType<BossCamera>().GetComponentInChildren<Camera>().transform, true);
                TimeUtils.timeScaleToggle();

            }
        }
    }
}