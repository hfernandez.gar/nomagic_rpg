﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Game {
    public class DestroyThingByPlayer : MonoBehaviour {

        // Use this for initialization
        void Start() {
            GameManager.instance.notifyThingObservers += OnPlayerAttackThing;
        }

        private void OnPlayerAttackThing(string typeOfAttack, string thingName)
        {
            if (!gameObject.name.Equals(thingName))
            {
                return;
            }

            if (typeOfAttack == "AOE")
            {
                GameManager.instance.notifyThingObservers -= OnPlayerAttackThing;
                Destroy(gameObject);
            }
        }

       
    }
}
