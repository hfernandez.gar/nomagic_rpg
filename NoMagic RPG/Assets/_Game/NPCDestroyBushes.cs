﻿using RPG.Cameras;
using RPG.Characters;
using RPG.Characters.Player;
using RPG.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Game
{
    public class NPCDestroyBushes : MonoBehaviour
    {
        Player player;
        WeaponSystem playerWeapon;

        //TODO Change to a combobox and just get the id
        [SerializeField]
        int weaponIdToCheck;
        [SerializeField]
        int yesNodeId;
        [SerializeField]
        int noNodeId;
        [SerializeField]
        GameObject fireParticles;

        float PARTICLE_CLEAN_UP_DELAY = 3f;


        public void CheckIfWeapon()
        {
            var weaponPlayerStrings = GameManager.instance.totalWeapons[GameManager.instance.selectedWeapon];
            var weaponToCheckStrings = GameManager.instance.totalWeapons[weaponIdToCheck];

            if (weaponPlayerStrings.name.Equals(weaponToCheckStrings.name))
            {
                UIManager.instance.SetNode(yesNodeId);
            }
            else
            {
                UIManager.instance.SetNode(noNodeId);
            }
        }

        public void TakeWeaponFromPlayer()
        {
            //Take weapon from player and maybe add that weapon to the NPC
            player.GetComponent<WeaponSystem>().TakeWeaponInHand();
            //Move the camera
            ToggleCameraTarget(FindObjectOfType<BushesCamera>().GetComponentInChildren<Camera>().transform, true);
            //Use Flame Special Ability and burn the bushes
            BurnBushes();
        }

        private void BurnBushes()
        {
            
            var bushes = FindObjectsOfType<DestroyThingByPlayer>();
            int middlemostBushIndex = bushes.Length / 2;


            Instantiate(fireParticles, bushes[middlemostBushIndex].transform);
            var fire = Instantiate(fireParticles,
               bushes[middlemostBushIndex].transform.position,
               fireParticles.transform.rotation);
            fire.transform.localPosition += fireParticles.transform.localPosition; //add position of original prefab
            fire.transform.parent = transform; //set world space in prefab if required
            StartCoroutine(DestroyParticleWhenFinished(fire, bushes));
        }

        IEnumerator DestroyParticleWhenFinished(GameObject particlePrefab, DestroyThingByPlayer[] bushes)
        {
            if (particlePrefab.GetComponentInChildren<ParticleSystem>() != null)
            {

                if (!particlePrefab.GetComponentInChildren<ParticleSystem>().isPlaying)
                {
                    particlePrefab.GetComponentInChildren<ParticleSystem>().Play();
                }
                while (particlePrefab.GetComponentInChildren<ParticleSystem>().isPlaying)
                {
                    yield return new WaitForSeconds(PARTICLE_CLEAN_UP_DELAY);
                }
            }

            for (int i = 0; i < bushes.Length; i++)
            {
                GameManager.instance.OnPlayerAttackOnThing("AOE", bushes[i].name);
            }

            Destroy(particlePrefab);

            //Move to original target
            ToggleCameraTarget(FindObjectOfType<Player>().transform, false);
            //Call next node of the dialogue
            UIManager.instance.CallNext();

            yield return new WaitForEndOfFrame();
        }

        private void ToggleCameraTarget(Transform target, bool isCameraRotationLocked)
        {
            FindObjectOfType<FreeLookCam>().SetTarget(target);
            FindObjectOfType<FreeLookCam>().SetLockCameraRotation(isCameraRotationLocked);
        }

        // Use this for initialization
        void Start()
        {
            player = FindObjectOfType<Player>();
            playerWeapon = player.GetComponent<WeaponSystem>();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
