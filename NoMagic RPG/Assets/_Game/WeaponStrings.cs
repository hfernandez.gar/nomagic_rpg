﻿using System;

namespace RPG.Game
{
    [Serializable]
    public class WeaponStrings
    {
        public string name;
        public string path;

        public WeaponStrings(string name, string path)
        {
            this.name = name;
            this.path = path;
        }
    }
}
