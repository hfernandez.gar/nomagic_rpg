﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace RPG.Game
{
    [Serializable]
    public class GameData
    {

        //Initialize
        public bool isGameStartedFirstTime;

        //Index 
        public int selectedWeapon;

        public Dictionary<int,WeaponStrings> weapons;
        

    }
}
