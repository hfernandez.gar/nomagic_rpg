﻿using RPG.Characters;
using RPG.Game;
using RPG.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RPG.Characters.Player
{

    public class SpecialWeaponPoint : MonoBehaviour
    {

        [SerializeField]
        WeaponConfig specialWeaponToSpawn;
        [SerializeField]
        SpecialAbilityConfig specialAbilityConfig;
        [SerializeField]
        GameObject specialWeaponParticles;

        GameObject specialWeapon;

        Player player;

        // Use this for initialization
        void Start()
        {
            player = FindObjectOfType<Player>();
            specialWeapon = Instantiate(specialWeaponToSpawn.GetWeaponPrefab(), transform);
            Instantiate(specialWeaponParticles, specialWeapon.transform);
            if (specialWeapon.GetComponent<BoxCollider>() != null)
                Destroy(specialWeapon.GetComponent<BoxCollider>());
        }

        void Update()
        {
            if(GetComponent<VIDE_Assign>().interactionCount >= 1)
            {
                player.AddSpecialAbility(specialAbilityConfig);
                TimeUtils.timeScaleToggle();
                Destroy(gameObject);
            }
        }

        void OnTriggerEnter(Collider col)
        {
            if (col.gameObject.tag == "Player")
            {

                Destroy(specialWeapon);
                UIManager.instance.Interact(col.gameObject.GetComponent<Player>().GetComponent<VIDE_Assign>());
                TimeUtils.timeScaleToggle();
            }
        }
    }
}
