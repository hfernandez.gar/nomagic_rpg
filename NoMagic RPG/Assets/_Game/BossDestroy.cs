﻿using RPG.Characters.Enemy;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RPG.Game
{
    public class BossDestroy : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (!FindObjectOfType<Boss>())
                StartCoroutine(WaitForReload());
        }

        IEnumerator WaitForReload()
        {
            yield return new WaitForSeconds(5);
            SceneManager.LoadScene(0);
        }
    }
}
