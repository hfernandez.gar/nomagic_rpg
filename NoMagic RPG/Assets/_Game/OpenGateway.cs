﻿using System.Collections;
using System.Collections.Generic;
using RPG.Cameras;
using RPG.Characters.Player;
using UnityEngine;

namespace RPG.Game { 
    public class OpenGateway : MonoBehaviour {

        [SerializeField]
        GameObject[] gates;
        [SerializeField]
        bool displayEnemies;
        [SerializeField]
        GameObject enemiesContainer;

        public void Open()
        {
            
            StartCoroutine(Op_Gateway());


        }

        private void ToggleCameraTarget(Transform target, bool isCameraRotationLocked)
        {
            FindObjectOfType<FreeLookCam>().SetTarget(target);
            FindObjectOfType<FreeLookCam>().SetLockCameraRotation(isCameraRotationLocked);
        }

        IEnumerator Op_Gateway()
        {
            ToggleCameraTarget(FindObjectOfType<GatewayCamera>().GetComponentInChildren<Camera>().transform, true);
            foreach (GameObject go in gates)
            {
                go.SetActive(false);
            }
            if (this.displayEnemies)
            {
                enemiesContainer.SetActive(true);
            }
            yield return new WaitForSeconds(5);
            ToggleCameraTarget(FindObjectOfType<Player>().transform, false);
        }
    }
}
