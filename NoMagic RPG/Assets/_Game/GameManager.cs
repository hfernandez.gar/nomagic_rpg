﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace RPG.Game
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;

        private GameData gameData;

        [HideInInspector]
        public Dictionary<int,WeaponStrings> weapons {
            get; set; }

        [HideInInspector]
        public Dictionary<int, WeaponStrings> totalWeapons { get; set; }

        public bool isGameStartedFirstTime;
        [HideInInspector]
        public int selectedWeapon;

        [HideInInspector]
        public CursorLockMode previousCursorLockState;
        [HideInInspector]
        public bool isPreviousFullscreen;


        public delegate void OnPlayerWeaponChange(int weaponIndex);
        public event OnPlayerWeaponChange notifyWeaponObservers;

        public delegate void OnPlayerWeaponGet(int weaponIndex);
        public event OnPlayerWeaponGet notifyPlayerWeaponGetObservers;

        public delegate void OnPlayerAttackEnemy(string enemyName);
        public event OnPlayerAttackEnemy notifyEnemyObservers;

        public delegate void OnPlayerDestroyThing(string typeOfAttack, string objectName);
        public event OnPlayerDestroyThing notifyThingObservers;


        public delegate void OnWindowFullscreenToogle();
        public event OnWindowFullscreenToogle notifyFullscreenObservers;

        


        void Awake()
        {
            weapons = new Dictionary<int, WeaponStrings>();
            totalWeapons = new Dictionary<int, WeaponStrings>();
            isPreviousFullscreen = Screen.fullScreen;
            MakeSigleton();            
            InitializeGame();
        }

        void MakeSigleton()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
           
        }

        void InitializeGame()
        {

            LoadGameData();

            TextAsset json = Resources.Load<TextAsset>("Text/PlayerWeapons");
            WeaponsJson[] weaponsjson = JsonHelper.FromJson<WeaponsJson>(json.text);

            foreach (var weapon in weaponsjson)
            {
                totalWeapons.Add(weapon.id, new WeaponStrings(weapon.name, weapon.path));
            }

            if (gameData != null)
            {
                isGameStartedFirstTime = gameData.isGameStartedFirstTime;
            }
            else
            {
                isGameStartedFirstTime = true;
            }

            if (isGameStartedFirstTime)
            {
                isGameStartedFirstTime = false;

                //selectedWeapon = 3; //index for the mop

                foreach (WeaponsJson weapon in weaponsjson)
                {
                    
                    if (weapon.id == selectedWeapon)
                    {
                        weapons.Add(weapon.id, new WeaponStrings(weapon.name, weapon.path));
                        break;
                    }
                }
                
                gameData = new GameData();

                SaveGameData();
                LoadGameData();
            }
            else
            {
               selectedWeapon = gameData.selectedWeapon;
            }

            
        }

        public void SaveGameData()
        {
            BinaryFormatter bf = new BinaryFormatter();



            using (FileStream file = File.Create(Application.persistentDataPath + "/GameData.dat"))
            {
                if (gameData != null)
                {
                    gameData.isGameStartedFirstTime = isGameStartedFirstTime;
                    gameData.weapons = weapons;
                    gameData.selectedWeapon = selectedWeapon;

                    bf.Serialize(file, gameData);
                }
            }
        }

        void LoadGameData()
        {
            BinaryFormatter bf = new BinaryFormatter();
            if (File.Exists(Application.persistentDataPath + "/GameData.dat"))
            {
                using (FileStream file = File.Open(Application.persistentDataPath + "/GameData.dat", FileMode.Open))
                {

                    try
                    {
                        gameData = (GameData)bf.Deserialize(file);

                        if (gameData != null)
                        {
                            isGameStartedFirstTime = gameData.isGameStartedFirstTime;
                            weapons = gameData.weapons;
                            selectedWeapon = gameData.selectedWeapon;
                        }
                    }
                    catch (EndOfStreamException ex)
                    {
                        Debug.Log("Delete GameData.dat and run the game again");
                        Debug.Log(ex.Message);
                    }
                }
            }
        }

        // Update is called once per frame
        void Update()
        {
            OnWindowFullscreenToogleFunction();
        }
    

        #region Events
        public void OnPlayerWeaponOnChange(int weaponIndex)
        {
            notifyWeaponObservers(weaponIndex);
        }

        public void OnPlayerWeaponOnGet(int weaponIndex)
        {
            notifyPlayerWeaponGetObservers(weaponIndex);
        }

        public void OnPlayerAttackOnEnemy(string enemy)
        {
            notifyEnemyObservers(enemy);
        }

        public void OnPlayerAttackOnThing(string attack, string objectName)
        {
            notifyThingObservers(attack, objectName);
        }

        public void OnWindowFullscreen()
        {
            notifyFullscreenObservers();
        }



        #endregion

        private void OnWindowFullscreenToogleFunction()
        {          
            if (previousCursorLockState == Cursor.lockState && isPreviousFullscreen == Screen.fullScreen)
                return;

            if (Screen.fullScreen)
                Cursor.lockState = CursorLockMode.Confined;
            else
                Cursor.lockState = CursorLockMode.None;

            previousCursorLockState = Cursor.lockState;
            isPreviousFullscreen = Screen.fullScreen;
        }



    }

    [Serializable]
    public class WeaponsJson
    {
        public int id;
        public string name;
        public string path;
    }

    [Serializable]
    public class Weapons
    {
        public List<WeaponsJson> weaponsJson;
    }


    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper);
        }

        public static string ToJson<T>(T[] array, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }

    public static class TimeUtils
    {
        public static GameState timeScaleToggle()
        {
            if (Time.timeScale >= 1.0f)
            {
                Time.timeScale = 0.0f;
                return GameState.Paused;
            }
            else
            {
                Time.timeScale = 1.0f;
                return GameState.Resume;
            }
        }

        public enum GameState
        {
            Paused,
            Resume
        }

        
        public static IEnumerator WaitForRealSeconds(float time)
        {
            float start = Time.realtimeSinceStartup;
            while (Time.realtimeSinceStartup < start + time)
            {
                yield return null;
            }
        }
        
    }
}
