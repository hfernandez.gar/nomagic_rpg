﻿using RPG.Characters;
using RPG.Characters.Enemy;
using RPG.Characters.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Weapon {

    public class WeaponContainerEnemyGroup : MonoBehaviour {

        [SerializeField]
        GameObject enemyGroupContainer;

        [SerializeField]
        GameObject weaponPrizeContainer;

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

           
            if (enemyGroupContainer.GetComponentsInChildren<EnemyAI>().Length == 0)
            {
                if (weaponPrizeContainer != null)
                {
                    if (!weaponPrizeContainer.activeSelf)
                    {
                        weaponPrizeContainer.GetComponent<SpecialWeaponPoint>().gameObject.SetActive(true);
                    }
                }
            }
                
            

            

        }
    }
}