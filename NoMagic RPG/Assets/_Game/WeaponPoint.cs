﻿using RPG.Game;
using System.Collections;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Characters
{
    public class WeaponPoint : MonoBehaviour
    {
        

        [SerializeField]
        WeaponConfig[] weaponsToSpawn;
        WeaponConfig weaponToSpawn;
        [SerializeField]
        GameObject weaponNotification;
        [SerializeField]
        GameObject weaponParticles;

        int weaponIndex;
        GameObject weapon;
        StringBuilder sb;

        GameObject weaponNotificationInstantiate;

        // Use this for initialization
        void Start()
        {
            
            sb = new StringBuilder();
            weaponIndex = Random.Range(0, weaponsToSpawn.Length);
            weaponToSpawn = weaponsToSpawn[weaponIndex];
            weapon = Instantiate(weaponToSpawn.GetWeaponPrefab(), transform);
            Instantiate(weaponParticles, weapon.transform);
            if(weapon.GetComponent<BoxCollider>() != null)
                Destroy(weapon.GetComponent<BoxCollider>());
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        void OnTriggerEnter(Collider col)
        {
            if (col.gameObject.tag == "Player")
            {
                var weaponId = weaponToSpawn.GetWeaponId();

                if (!GameManager.instance.weapons.ContainsKey(weaponId))
                {

                    sb = sb.Remove(0, sb.Length);
                    sb.Append("You got ");
                    FindObjectOfType<Player.Player>().GetComponent<WeaponSystem>().PutWeaponInHand(weaponToSpawn);

                    GameManager.instance.selectedWeapon = weaponId;

                    var weaponStrings = GameManager.instance.totalWeapons[GameManager.instance.selectedWeapon];

                    GameManager.instance.weapons.Add(weaponId, new WeaponStrings(weaponStrings.name, weaponStrings.path));
                    GameManager.instance.OnPlayerWeaponOnGet(weaponId);

                    string nameWeapon = weaponToSpawn.GetWeaponPrefab().name;
                    //English
                    switch (nameWeapon.ToLower()[0])
                    {
                        case 'a':
                        case 'e':
                        case 'i':
                        case 'o':
                        case 'u':
                            sb.Append("an " + nameWeapon);
                            break;

                        default:
                            sb.Append("a " + nameWeapon);
                            break;

                    }

                    Destroy(weapon);
                    weaponNotification.GetComponent<TextMeshProUGUI>().text = sb.ToString();
                    weaponNotificationInstantiate = Instantiate(weaponNotification, FindObjectOfType<HUD>().transform);
                    StartCoroutine(DisplayNotification());
                }
                else
                {
                    Destroy(gameObject);
                }

            }
        }

        IEnumerator DisplayNotification()
        {
            weaponNotificationInstantiate.SetActive(true);
            yield return new WaitForSecondsRealtime(2);
            Destroy(weaponNotificationInstantiate);
            Destroy(gameObject);    
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(transform.position, Vector3.one);
        }
    }
}
