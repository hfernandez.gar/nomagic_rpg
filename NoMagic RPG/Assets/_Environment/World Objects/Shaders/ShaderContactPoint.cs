﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderContactPoint : MonoBehaviour
{
 
    [SerializeField]   
    private Material shaderMaterial;

    public int duration = 10;
    public float distance = 3;
    public Transform targetTransform;

    private float lastTimeHit;


    void Start(){
        shaderMaterial.SetInt("_Duration", duration);
        shaderMaterial.SetFloat("_Distance", distance);
        shaderMaterial.SetVector("_targetPosition", Vector3.zero);
    }


    void OnCollisionEnter(Collision collision)
    {
        Vector3 collidePoint = GetComponent<Collider>().ClosestPointOnBounds(new 
            Vector3(collision.collider.bounds.center.x + collision.collider.contactOffset, 
            collision.collider.bounds.center.y + collision.collider.contactOffset, 
            collision.collider.bounds.center.z + collision.collider.contactOffset));

        //To transform to 0 to 1 range vector
        Vector3 localCollidePoint = transform.InverseTransformPoint(collidePoint);
     
        shaderMaterial.SetVector("_contactPoint", new Vector2(-localCollidePoint.x,-localCollidePoint.z));
        
        shaderMaterial.SetInt("_isContact", 1);
        
        lastTimeHit = Time.time;

    }

    void Update()
    {
        shaderMaterial.SetVector("_targetPosition", targetTransform.position);
        if (shaderMaterial.GetInt("_isContact") == 1 && Time.time - shaderMaterial.GetInt("_Duration") >= lastTimeHit)
        {
            shaderMaterial.SetInt("_isContact", 0);
        }
    }

}
