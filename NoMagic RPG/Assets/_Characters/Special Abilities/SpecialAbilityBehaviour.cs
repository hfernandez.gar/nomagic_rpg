﻿using System.Collections;
using UnityEngine;

namespace RPG.Characters {

    public abstract class SpecialAbilityBehaviour : MonoBehaviour {

        protected SpecialAbilityConfig config;

        const string ATTACK_TRIGGER = "Attack";
        const string DEFAULT_ATTACK_STATE = "Default Attack";
        const float PARTICLE_CLEAN_UP_DELAY = 5f;

        public abstract void Use(GameObject target = null);

        public void SetConfig(SpecialAbilityConfig config)
        {
            this.config = config;
        }

        protected void PlayParticleEffect()
        {
            GameObject particlePrefab = config.GetParticlePrefab();
            var particleObject = Instantiate(particlePrefab, 
                transform.position, 
                particlePrefab.transform.rotation);
            particleObject.transform.localPosition += particlePrefab.transform.localPosition; //add position of original prefab
            particleObject.transform.parent = transform; //set world space in prefab if required
            if (particleObject.GetComponentInChildren<ParticleSystem>() != null)
                particleObject.GetComponentInChildren<ParticleSystem>().Play();
            else
                particleObject.GetComponent<ParticleSystem>().Play();
            StartCoroutine(DestroyParticleWhenFinished(particleObject));

        }

        IEnumerator DestroyParticleWhenFinished(GameObject particlePrefab)
        {
            if (particlePrefab.GetComponentInChildren<ParticleSystem>() != null)
            {
                while (particlePrefab.GetComponentInChildren<ParticleSystem>().isPlaying)
                {
                    yield return new WaitForSeconds(PARTICLE_CLEAN_UP_DELAY);
                }
            }
            else
            {
                while (particlePrefab.GetComponent<ParticleSystem>().isPlaying)
                {
                    yield return new WaitForSeconds(PARTICLE_CLEAN_UP_DELAY);
                }

            }
            Destroy(particlePrefab);
            yield return new WaitForEndOfFrame();
        }

        protected void PlayAbilityAnimation()
        {
            var animatorOverrideController = GetComponent<Character>().GetOverrideController();
            var animator = GetComponent<Animator>();
            animator.runtimeAnimatorController = animatorOverrideController;
            animatorOverrideController[DEFAULT_ATTACK_STATE] = config.GetAnimation();
            animator.SetTrigger(ATTACK_TRIGGER);
        }

        protected void PlayAbilitySound()
        {
            var abilitySound = config.GetRandomAbilitySound();
            var audioSource = GetComponent<AudioSource>();
            audioSource.PlayOneShot(abilitySound);
        }

    }
}
