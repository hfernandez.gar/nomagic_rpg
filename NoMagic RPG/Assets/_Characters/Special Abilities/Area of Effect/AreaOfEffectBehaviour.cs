﻿using RPG.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using RPG.Game;

namespace RPG.Characters
{

    public class AreaOfEffectBehaviour : SpecialAbilityBehaviour
    {
        private const int PLAYER_LAYER = 11;

        public override void Use(GameObject target)
        {
            DealRadialDamage();
            PlayParticleEffect();
            PlayAbilitySound();
            PlayAbilityAnimation();
        }

        private void DealRadialDamage()
        {
            var hitsAOE = Physics.SphereCastAll(
                gameObject.transform.position,
                (config as AreaOfEffectConfig).GetRadius(),
                Vector3.up,
                (config as AreaOfEffectConfig).GetRadius()
                );


            foreach (RaycastHit hit in hitsAOE)
            {
                var damageable = hit.collider.gameObject.GetComponent<HealthSystem>();
                if (damageable != null && hit.collider.gameObject.layer != PLAYER_LAYER)
                {
                    float damageToDeal = (config as AreaOfEffectConfig).GetDamageToEachTarget();
                    damageable.TakeDamage(damageToDeal);
                    if (damageable.tag.Equals("Enemy"))
                    {
                        GameManager.instance.OnPlayerAttackOnEnemy(damageable.name);
                    }
                }

                
                if (hit.collider.gameObject.GetComponent<DestroyThingByPlayer>())
                {
                    var thing = hit.collider.gameObject.GetComponent<DestroyThingByPlayer>();
                    GameManager.instance.OnPlayerAttackOnThing("AOE", thing.name);
                }
            }
        }
    }
}
