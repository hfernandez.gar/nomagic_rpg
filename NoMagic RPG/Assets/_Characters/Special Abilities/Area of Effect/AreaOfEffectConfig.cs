﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Characters
{
    [CreateAssetMenu(menuName = ("RPG/Special Ability/Area Of Effect"))]
    public class AreaOfEffectConfig : SpecialAbilityConfig
    {
        [Header("Area Of Effect Specific")]
        [SerializeField]
        float radius = 5f;
        [SerializeField]
        float damageToEachTarget = 10f;

        public override SpecialAbilityBehaviour GetBehaviourComponent(GameObject objectToAttachToo)
        {
            return objectToAttachToo.AddComponent<AreaOfEffectBehaviour>();
        }

        public float GetDamageToEachTarget()
        {
            return damageToEachTarget;
        }

        public float GetRadius()
        {
            return radius;
        }
    }
}
