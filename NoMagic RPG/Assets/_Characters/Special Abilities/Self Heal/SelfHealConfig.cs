﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Characters
{
    [CreateAssetMenu(menuName = ("RPG/Special Ability/Self Heal"))]
    public class SelfHealConfig : SpecialAbilityConfig
    {
        [Header("Self Heal Specific")]
        [SerializeField]
        float recoverHP = 10f;

        
        public override SpecialAbilityBehaviour GetBehaviourComponent(GameObject objectToAttachToo)
        {
            return objectToAttachToo.AddComponent<SelfHealBehaviour>();
        }



        public float GetRecoverHP()
        {
            return recoverHP;
        }
    }
}

