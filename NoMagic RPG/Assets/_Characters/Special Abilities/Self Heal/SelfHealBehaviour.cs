﻿using System;
using UnityEngine;

namespace RPG.Characters
{
    public class SelfHealBehaviour : SpecialAbilityBehaviour
    {

  
        Player.Player player;
       
        public void Start()
        {
            player = GetComponent<Player.Player>();
        }

       
        public override void Use(GameObject target)
        {
            PlayParticleEffect();
            RecoverHealth();
            PlayAbilitySound();
            PlayAbilityAnimation();
        }

        private void RecoverHealth()
        {
            HealthSystem playerHeal = player.GetComponent<HealthSystem>();
            playerHeal.Heal((config as SelfHealConfig).GetRecoverHP());
        }

      

       
    }
}