﻿using RPG.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Characters
{

    public class PowerAttackBehaviour : SpecialAbilityBehaviour
    {

       

        public override void Use(GameObject target)
        {
            LookAt(target);
            PlayParticleEffect();
            DealDamage(target);
            PlayAbilitySound();
            PlayAbilityAnimation();
        }

        private void LookAt(GameObject target)
        {
            var particleSystem = config.GetParticlePrefab().GetComponentInChildren<ParticleSystem>();
            var parVel = particleSystem.velocityOverLifetime;

            particleSystem.transform.LookAt(target.transform, Vector3.up);
            AnimationCurve curveX = new AnimationCurve();
            AnimationCurve curveY = new AnimationCurve();
            AnimationCurve curveZ = new AnimationCurve();

            Vector3 unityVector = (target.transform.position - transform.position).normalized;

            Keyframe[] keyFrames = new Keyframe[3];
            keyFrames[0] = new Keyframe(0, 0);
            keyFrames[1] = new Keyframe(0.5f, 0);



            if (unityVector.x != 0)
            {
                var kf = new Keyframe(1, 15);
                kf.value *= Mathf.Sign(unityVector.x);
                keyFrames[2] = kf;
                curveX.keys = keyFrames;
                parVel.x = new ParticleSystem.MinMaxCurve(1, curveX);
            }

            if (unityVector.y != 0)
            {
                var kf = new Keyframe(1, 15);
                kf.value *= Mathf.Sign(unityVector.y);
                keyFrames[2] = kf;
                curveY.keys = keyFrames;
                parVel.y = new ParticleSystem.MinMaxCurve(1, curveY);
            }

            if (unityVector.z != 0)
            {
                var kf = new Keyframe(1, 15);
                kf.value *= Mathf.Sign(unityVector.z);
                keyFrames[2] = kf;
                curveZ.keys = keyFrames;
                parVel.z = new ParticleSystem.MinMaxCurve(1, curveZ);
            }

           

            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z), Vector3.up);
        }

        private void DealDamage(GameObject target)
        {
            float damageToDeal = (config as PowerAttackConfig).GetExtraDamage();
            target.GetComponent<HealthSystem>().TakeDamage(damageToDeal);
            if (target.tag.Equals("Enemy"))
            {
                GameManager.instance.OnPlayerAttackOnEnemy(target.name);
            }
        }

     
    }
}
