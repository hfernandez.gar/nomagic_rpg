﻿using RPG.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Characters
{ 
    public abstract class SpecialAbilityConfig : ScriptableObject
    {

        [Header("Special Ability General")]
        [SerializeField]
        float energyCost = 10f;
        [SerializeField]
        GameObject particlePrefab = null;
        [SerializeField]
        AnimationClip specialAbilityAnimation = null;
        [SerializeField]
        AudioClip[] audioClips = null;
        [SerializeField]
        Sprite sprite;

        protected SpecialAbilityBehaviour behaviour;

        public abstract SpecialAbilityBehaviour GetBehaviourComponent(GameObject objectToAttachTo);

        public void AttachAbilityTo(GameObject objectToAttachTo)
        {
            SpecialAbilityBehaviour behaviourComponent = GetBehaviourComponent(objectToAttachTo);
            behaviourComponent.SetConfig(this);
            behaviour = behaviourComponent;
        }

        public void Use(GameObject target)
        {
            behaviour.Use(target);
        }


        public float GetEnergyCost()
        {
            return energyCost;
        }

        public GameObject GetParticlePrefab()
        {
            return particlePrefab;
        }

        public AudioClip GetRandomAbilitySound()
        {
            return audioClips[Random.Range(0, audioClips.Length)];
        }

        public AnimationClip GetAnimation()
        {
            return specialAbilityAnimation;
        }

        public Sprite GetSprite()
        {
            return sprite;
        }

    }

    
}
