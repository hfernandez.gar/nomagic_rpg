﻿using RPG.Characters.Enemy;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Characters.NPC
{
    public class NPC : MonoBehaviour
    {

        Character character;
        EnemyAI enemyAI;

        // Use this for initialization
        void Start()
        {
            character = GetComponent<Character>();
            enemyAI = GetComponent<EnemyAI>(); 
        }

        // Update is called once per frame
        void Update()
        {
            

        }
    }
}
