﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Characters
{

    [CreateAssetMenu(menuName = ("RPG/Weapon"))]
    public class WeaponConfig : ScriptableObject
    {

        public Transform gripTransform;

        [SerializeField]
        int weaponId;
        [SerializeField]
        GameObject weaponPrefab;
        [SerializeField]
        GameObject weaponBullet;
        [SerializeField]
        AnimationClip attackAnimation;
        [SerializeField]
        float timeBetweenAnimationCycles = 0.5f;
        [SerializeField]
        float maxAttackRange = 1.5f;
        [SerializeField]
        float extraDamage = 10f;

        public int GetWeaponId()
        {
            return weaponId;
        }

        public float GetMinTimeBetweenAnimationCycles()
        {
            return timeBetweenAnimationCycles;
        }

        public float GetMaxAttackRange()
        {
            return maxAttackRange;
        }

        public GameObject GetWeaponPrefab()
        {
            return weaponPrefab;
        }

        public GameObject GetWeaponBullet()
        {
            return weaponBullet;
        }

        public AnimationClip GetAnimClip()
        {
            if(attackAnimation)
                RemoveAnimationEvents();
            return attackAnimation;
        }

        public float GetExtraDamage()
        {
            return extraDamage;
        }

        private void RemoveAnimationEvents()
        {
            attackAnimation.events = new AnimationEvent[0];
        }
    }
}
