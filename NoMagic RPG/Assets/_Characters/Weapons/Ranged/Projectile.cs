﻿using UnityEngine;
using RPG.Utilities;

namespace RPG.Characters
{
    public class Projectile : MonoBehaviour
    {

        float damageAmount;
        [SerializeField]
        float projectileSpeed;
        [SerializeField]
        GameObject shooter; //So can inspect when paused

        const float DESTROY_DELAY = 0.1f;

        public float GetDefaultProjectileSpeed()
        {
            return projectileSpeed;
        }

        public void SetDamage(float damage)
        {
            damageAmount = damage;
        }

        public void SetShooter(GameObject shooter)
        {
            this.shooter = shooter;
        }

        void OnTriggerEnter(Collider collider)
        {
            DamageIfDamageables(collider);
            Destroy(gameObject, DESTROY_DELAY);
        }

        private void DamageIfDamageables(Collider collider)
        {
            Component damageable = collider.gameObject.GetComponent<HealthSystem>();
            if (damageable && shooter.layer != collider.gameObject.layer)
            {
                (damageable as HealthSystem).TakeDamage(damageAmount);
            }
        }
    }
}
