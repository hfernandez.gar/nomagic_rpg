﻿using UnityEngine;

namespace RPG.Characters.Enemy
{
    public class FaceCamera : MonoBehaviour
    {
        Camera cameraToLookAt;
      
        // Use this for initialization 
        void Start()
        {
            cameraToLookAt = Camera.main;
        }

        // Update is called once per frame 
        void LateUpdate()
        {

            if (Camera.main != null)
            {
                cameraToLookAt = Camera.main;
                transform.LookAt(cameraToLookAt.transform);
            }
        }
    }
}