﻿using System.Collections;
using UnityEngine;
using RPG.Characters;
using RPG.Utilities;
using System;
using RPG.Game;
using VIDE_Data;

namespace RPG.Characters.Enemy
{
    #pragma warning disable
    [RequireComponent(typeof(HealthSystem))]
    [RequireComponent(typeof(WeaponSystem))]
    public class EnemyAI : MonoBehaviour
    {
        [SerializeField]
        float chaseRadius = 1f;
        [SerializeField]
        WaypointContainer patrolPath;
        [SerializeField]
        float waypointTolerance = 1f;
        [SerializeField]
        float waypointDwellTime = 2.0f;
        [SerializeField]
        float distanceCantAttackStaticEnemy = 3.046f;
        [SerializeField]
        float patrolAnimationSpeedMultiplier = 1.5f;
        [SerializeField]
        float chaseAnimationSpeedMultiplier = 1.5f;
        [SerializeField]
        GameObject weaponLootPrefab;

        float defaultAnimationSpeedMultiplier;
        
        private Player.Player player;
        Character character;
        EnemyMovement enemyMovement;

        int nextWaypointIndex = 0;
        float currentWeaponRange;
        float distanceToPlayer;
        float runValue = 0.0f;
        float originalAngleDeegres = 0.0f;
        Vector3 originalForward = Vector3.zero;
        Quaternion originalQuaternion = Quaternion.identity;

        WeaponSystem weaponSystem;
        bool wasAttackedWhilePatrolling = false;

        enum State
        {
            idle,
            attacking,
            patrolling,
            chasing,
            sleeping
        }

        State state = State.idle;

        void Awake()
        {
            character = GetComponent<Character>();

            defaultAnimationSpeedMultiplier = character.GetAnimationSpeedMultiplier();

            if(character.GetIsMobile())
                enemyMovement = GetComponent<EnemyMovement>();
         
            player = FindObjectOfType<Player.Player>();

            originalAngleDeegres = gameObject.transform.rotation.eulerAngles.y;
            originalForward = gameObject.transform.forward;
            originalQuaternion = gameObject.transform.rotation;

            weaponSystem = GetComponent<WeaponSystem>();

           
            
        }


        void Start()
        {
            if(gameObject.tag.Equals("Enemy"))
                GameManager.instance.notifyEnemyObservers += OnPlayerAttackEnemy;
        }


        void OnPlayerAttackEnemy(string enemyName)
        {
            if (!gameObject.name.Equals(enemyName))
            {
                return;
            }

            IsPlayerDead();

            distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);

            currentWeaponRange = weaponSystem.GetCurrentWeapon().GetMaxAttackRange();

            if (character.GetIsMobile() && (state == State.patrolling || state == State.idle))
            {
                StopAllCoroutines();
                weaponSystem.StopAttacking();
                wasAttackedWhilePatrolling = true;
                StartCoroutine(ChasePlayer());
            }
            
        }

        void IsPlayerDead()
        {
            if (player.GetComponent<HealthSystem>().healthAsPercentage() <= Mathf.Epsilon)
            {
                StopAllCoroutines();
                return;
            }

        }

        void Update()
        {
            if (VD.isActive)
            {
                InDialogue();
                return;
            }

            IsPlayerDead();

            distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);

            
            currentWeaponRange = weaponSystem.GetCurrentWeapon().GetMaxAttackRange();
			
            if (character.GetIsMobile())
            {
                bool inWeaponRing = distanceToPlayer <= currentWeaponRange;
                bool inChaseRing = distanceToPlayer <= chaseRadius;
                bool outsideChaseRing = distanceToPlayer > chaseRadius;

                if (!wasAttackedWhilePatrolling)
                {
 

                    if (outsideChaseRing && state != State.patrolling)
                    {
                        StopAllCoroutines();
                        weaponSystem.StopAttacking();
                        StartCoroutine(Patrol());
                    }

                    if (inChaseRing && state != State.chasing)
                    {
                        StopAllCoroutines();
                        weaponSystem.StopAttacking();
                        StartCoroutine(ChasePlayer());
                    }


                }

                if (inWeaponRing)
                {
                    StopAllCoroutines();
                    weaponSystem.AttackTarget(player.gameObject);
                    wasAttackedWhilePatrolling = false;
                }
            }
            else
            {
                Vector3 targetDir = player.transform.position - transform.position;

                float minAngle = originalAngleDeegres - 45.0f;
                float maxAngle = originalAngleDeegres + 45.0f;
                float moveAngle = gameObject.transform.rotation.eulerAngles.y;
                float playerAngle = Vector3.Angle(player.transform.position, originalForward);

                bool inWeaponCannon = distanceToPlayer <= currentWeaponRange && distanceToPlayer >= distanceCantAttackStaticEnemy;
                bool isInRangeAngle = moveAngle >= minAngle && moveAngle <= maxAngle;

                var targetQuaternion = Quaternion.identity;

                
                targetDir.y = 0;

                Quaternion q = Quaternion.LookRotation(targetDir);
                var a = Quaternion.Angle(q, originalQuaternion);
                if (a <= maxAngle)
                {
                    targetQuaternion = q;
                }

               
               
                if (inWeaponCannon && isInRangeAngle)
                {
                   state = State.attacking;
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetQuaternion, Time.deltaTime * 2);
                    weaponSystem.AttackTarget(player.gameObject);
                }
                else if (!inWeaponCannon || !isInRangeAngle && state != State.idle)
                {
                    state = State.idle;
                    transform.rotation = Quaternion.Slerp(transform.rotation, originalQuaternion, Time.deltaTime * 2);
                    weaponSystem.StopAttacking();
                }
            }
        }

        public void Kill()
        {
            //kill signaling
            //If enemy has weapon loot, instantiate it when it dies
            if (weaponLootPrefab)
            {
                Instantiate(weaponLootPrefab, gameObject.transform.position, Quaternion.identity).SetActive(true);
            }
            StopAllCoroutines();
            if (gameObject.tag.Equals("Enemy"))
                GameManager.instance.notifyEnemyObservers -= OnPlayerAttackEnemy;
        }

        public void InDialogue()
        {
            if (character.GetIsMobile())
            {
                character.GetAnimator().SetFloat("Run", 0.0f);
                enemyMovement.LookAt(player.transform.position);
            }
            weaponSystem.StopAttacking();
        }

        IEnumerator ChasePlayer()
        {
            state = State.chasing;
            while (distanceToPlayer >= currentWeaponRange)
            {
                //if(VD.isActive) { break; }
                runValue = 1.0f;
                UpdateAnimator("Run",runValue,chaseAnimationSpeedMultiplier);
                enemyMovement.SetDestination(player.transform.position);
                yield return new WaitForEndOfFrame();
            }

        }
        
        IEnumerator Patrol()
        {
            state = State.patrolling;
            while (patrolPath != null)
            {
                Vector3 nextWaypointPos = patrolPath.transform.GetChild(nextWaypointIndex).position;
                enemyMovement.SetDestination(nextWaypointPos);
                runValue = 0.5f;
                UpdateAnimator("Run", runValue, patrolAnimationSpeedMultiplier);
                CycleWaypointWhenClose(nextWaypointPos);
                yield return new WaitForSeconds(waypointDwellTime);
            }
        }

        private void CycleWaypointWhenClose(Vector3 nextWaypointPos)
        {
            if (Vector3.Distance(transform.position, nextWaypointPos) <= waypointTolerance)
            {
                runValue = 0.5f;
                UpdateAnimator("Run", runValue, defaultAnimationSpeedMultiplier);
                nextWaypointIndex = (nextWaypointIndex + 1) % patrolPath.transform.childCount;
            }
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, currentWeaponRange);
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, chaseRadius);
           
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, distanceCantAttackStaticEnemy);
            /*Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, (transform.forward - transform.right));
            Gizmos.DrawLine(transform.position, (transform.forward - -transform.right));*/
            
                      
        }

        
        void UpdateAnimator(string floatValue,float runValue, float animationSpeedMultiplier)
        {
            
            character.GetAnimator().SetFloat(floatValue, runValue, 0.05f, Time.deltaTime);
            character.GetAnimator().speed = animationSpeedMultiplier;
        }
    }

}
