﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace RPG.Characters.Enemy
{
    public class EnemyMovement : MonoBehaviour
    {
        
        [Header("Nav Mesh Agent")]
        [SerializeField]
        float navMeshAgentSteeringSpeed = 1.0f;
        [SerializeField]
        float navMeshAgentStoppingDistance = 1.3f;
        [SerializeField]
        float navMeshAgentBaseOffset = 0f;
        [SerializeField]
        float navMeshAgentRadius = 0.5f;
        [SerializeField]
        float navMeshAgentHeight = 2f;
        [SerializeField]
        float rotationSpeed = 40.5f;




        NavMeshAgent navMeshAgent;

        // Use this for initialization
        void Awake()
        {

            navMeshAgent = gameObject.AddComponent<NavMeshAgent>();

            navMeshAgent.speed = navMeshAgentSteeringSpeed;
            navMeshAgent.stoppingDistance = navMeshAgentStoppingDistance;
            navMeshAgent.baseOffset = navMeshAgentBaseOffset;
            navMeshAgent.radius = navMeshAgentRadius;
            navMeshAgent.height = navMeshAgentHeight;
            navMeshAgent.autoBraking = false;
            navMeshAgent.updateRotation = false;
            navMeshAgent.updatePosition = true;

            if (navMeshAgent.baseOffset != 0)
                navMeshAgent.Warp(transform.position);
        }

        public void SetDestination(Vector3 worldPos)
        {
            navMeshAgent.SetDestination(worldPos);
            
            Vector3 direction = (worldPos - transform.position).normalized;
            //This enables to only rotate around the y axis
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
        }

        public void LookAt(Vector3 worldPos)
        {
            //TODO Implement better rotation to stop jittery
            navMeshAgent.ResetPath();

            Vector3 direction = (worldPos - transform.position).normalized;
            //This enables to only rotate around the y axis
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
        }
    }
}