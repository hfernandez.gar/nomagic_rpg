﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RPG.Characters.Player;
using UnityEngine.SceneManagement;

namespace RPG.Characters
{
    public class HealthSystem : MonoBehaviour
    {

        private const string DAMAGE_TRIGGER = "Damage";
        private const string DEATH_TRIGGER = "Death";


        [SerializeField]
        float maxHealthPoints = 100f;
        [SerializeField]
        float deathDelay = 3.0f;
        [SerializeField]
        RawImage healthBar;
        [SerializeField]
        AudioClip[] damageSounds, deathSounds;

        float currentHealthPoints;
        Animator animator;
        AudioSource audioSource;
        PlayerMovement playerMovement;

        // Use this for initialization
        void Start()
        {
            animator = GetComponent<Animator>();
            audioSource = GetComponent<AudioSource>();
            playerMovement = GetComponent<PlayerMovement>();

            currentHealthPoints = maxHealthPoints;
        }

        // Update is called once per frame
        void Update()
        {
            UpdateHealthBar();
        }

        void UpdateHealthBar()
        {
            if (healthBar)
            {
                float xValue = -(healthAsPercentage() / 2f) - 0.5f;
                healthBar.uvRect = new Rect(xValue, 0f, 0.5f, 1f);
            }
        }

        public void TakeDamage(float damage)
        {
            bool characterDies = currentHealthPoints - damage <= 0; //must ask before reducing health
            currentHealthPoints = Mathf.Clamp(currentHealthPoints - damage, 0f, maxHealthPoints);

            if (characterDies) //Player dies
            {
                StartCoroutine(KillCharacter());
            }
            else
            {
                animator.SetTrigger(DAMAGE_TRIGGER);
                AudioClip clip = damageSounds[Random.Range(0, damageSounds.Length)];
                audioSource.PlayOneShot(clip);

            }

        }

        public void Heal(float points)
        {
            currentHealthPoints = Mathf.Clamp(currentHealthPoints + points, 0f, maxHealthPoints);
        }



        IEnumerator KillCharacter()
        {

            animator.SetTrigger(DEATH_TRIGGER);
            Player.Player playerComponent = GetComponent<Player.Player>();
            Enemy.EnemyAI enemyComponent = GetComponent<Enemy.EnemyAI>();
            audioSource.clip = deathSounds[Random.Range(0, deathSounds.Length)];
            audioSource.Play(); //override any existing sounds

            yield return new WaitForSecondsRealtime(audioSource.clip.length);


            if (playerComponent) //relying on lazy evaluation
            {
                if (playerComponent.isActiveAndEnabled)
                {
                    playerMovement.Kill();
                    yield return new WaitForSecondsRealtime(animator.GetCurrentAnimatorStateInfo(0).length); //Wait until death animation clip finishes playing.
                    SceneManager.LoadScene(0);
                }
            }

            if (enemyComponent)
            {
                if (enemyComponent.isActiveAndEnabled)
                {
                    enemyComponent.Kill();
                    Destroy(gameObject, deathDelay);
                }
            }

            

   
        }

        public float healthAsPercentage()
        {
            return currentHealthPoints / (float)maxHealthPoints;
        }
    }
}