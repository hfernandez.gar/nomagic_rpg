﻿using UnityEngine;
using UnityEngine.UI;
using RPG.Cameras;
using RPG.Characters.Player;
using RPG.Characters.Enemy;
using System;
using System.Collections;
using System.Collections.Generic;

namespace RPG.Characters
{
    public class SpecialAbilities : MonoBehaviour
    {
        //Temporarily serialized for debugging
        [SerializeField]
        List<SpecialAbilityConfig> abilities;

        [SerializeField]
        RawImage energyBar;
        [SerializeField]
        float maxEnergyPoints = 100f;
        [SerializeField]
        float regenEnergyPointsPerSecond = 10f;
        [SerializeField]
        AudioClip outOfEnergy;



        float currentEnergyPoints;
        AudioSource audioSource;

        float EnergyAsPercent()
        {
            return currentEnergyPoints / (float)maxEnergyPoints;
        }
        // Use this for initialization
        void Start()
        {
            audioSource = GetComponent<AudioSource>();
            currentEnergyPoints = maxEnergyPoints;
            AttachInitialAbilities();
            UpdateEnergyBar();
        }

        public int GetNumberAbilities()
        {
            return abilities.Count;
        }

        public SpecialAbilityConfig GetSpecialAbilityByIndex(int index)
        {
            return abilities[index];
        }

        public bool isAbilityIndexSelfHealAbility(int index)
        {
            return abilities[index].GetType() == typeof(SelfHealConfig);
        }

        public void AddSpecialAbility(SpecialAbilityConfig specialAbility)
        {
            this.abilities.Add(specialAbility);
            AttachNewAbility();
        }

        public float GetSpecialAbilityRange(int index)
        {
            if (this.abilities[index] is AreaOfEffectConfig)
                return (this.abilities[index] as AreaOfEffectConfig).GetRadius();
            else
                return 0;
        }

        public List<Sprite> GetAbilitiesSprites()
        {
            List<Sprite> sprites = new List<Sprite>();

            foreach(SpecialAbilityConfig so in this.abilities)
            {
                sprites.Add(so.GetSprite());
            }

            return sprites;
        }

        private void AttachInitialAbilities()
        {
            for (int i = 0; i < abilities.Count; i++)
            {
                abilities[i].AttachAbilityTo(gameObject);
            }
        }

        private void AttachNewAbility()
        {
            abilities[abilities.Count].AttachAbilityTo(gameObject);
        }

        public void AttemptSpecialAbility(int index, GameObject target = null)
        {
 
            var energyCost = abilities[index].GetEnergyCost();
            if (energyCost <= currentEnergyPoints)
            {
                ConsumeEnergy(energyCost);
                print("Using special ability " + index);
                abilities[index].Use(target);
            }
            else
            {
                audioSource.PlayOneShot(outOfEnergy);
            }
        }

        public void ConsumeEnergy(float amount)
        {
            float newCurrentEnergyPoints = currentEnergyPoints - amount;
            currentEnergyPoints = Mathf.Clamp(newCurrentEnergyPoints, 0, maxEnergyPoints);
            UpdateEnergyBar();
        }

        private void UpdateEnergyBar()
        {
            if (energyBar)
            {
                float xValue = -(EnergyAsPercent() / 2f) - 0.5f;
                energyBar.uvRect = new Rect(xValue, 0f, 0.5f, 1f);
            }
        }

  

        void Update()
        {
            if (currentEnergyPoints < maxEnergyPoints)
            {
                AddEnergyPoints();
                UpdateEnergyBar();
            }
        }

        private void AddEnergyPoints()
        {
            var pointsToAdd = regenEnergyPointsPerSecond * Time.deltaTime;
            currentEnergyPoints += pointsToAdd;
        }

    }
}