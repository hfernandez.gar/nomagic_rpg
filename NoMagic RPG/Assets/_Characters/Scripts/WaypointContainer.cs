﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Characters.Enemy
{
    public class WaypointContainer : MonoBehaviour
    {
        private void OnDrawGizmos()
        {
            Vector3 firstPosition = transform.GetChild(0).transform.position;
            Vector3 previousPosition = firstPosition;
            foreach(Transform waypoint in transform)
            {
                Gizmos.DrawSphere(waypoint.transform.position, 0.2f);
                Gizmos.DrawLine(previousPosition, waypoint.position);
                previousPosition = waypoint.position;
            }
            Gizmos.DrawLine(previousPosition, firstPosition);
        }
        
    }
}
