﻿using System;
using UnityEngine;
using UnityEngine.AI;
using RPG.Cameras;

namespace RPG.Characters
{
    [SelectionBase]
    public class Character : MonoBehaviour
    {

        private const int PLAYER_LAYER = 11;

        [SerializeField]
        bool isMobile = true;

        [Header("Rigidbody")]
        [SerializeField]
        float mass = 1f;

        [Header("Animator")]
        [SerializeField]
        RuntimeAnimatorController animatorController;
        [SerializeField]
        AnimatorOverrideController animatorOverrideController;
        [SerializeField]
        Avatar characterAvatar;
        [SerializeField]
        AudioClip footStepSound;
        //Add more clips if needed

        [Header("Audio")]
        [SerializeField]
        float audioSourceSpatialBlend = 0.5f;


        [Header("Movement")]
        [SerializeField]
        float moveSpeedMultiplier = .7f;
        [SerializeField]
        float animationSpeedMultiplier = 1.5f;
        [SerializeField]
        float movingTurnSpeed = 360;
        [SerializeField]
        float stationaryTurnSpeed = 180;
        [SerializeField]
        float moveThreshold = 1f;

        float turnAmount;
        float forwardAmount;


        Animator animator;
        Rigidbody rigidBody;
        AudioSource audioSource;

        bool isAlive;


        void Awake()
        {
            if (!isMobile)
            {
                if (!gameObject.GetComponentInChildren<Weakpoint>())
                {
                    Debug.Break();
                    Debug.LogAssertion("Please provide " + gameObject + " with a weakpoint game object.");
                }
            }
            AddRequiredComponents();
            isAlive = true;
        }

        public bool GetIsMobile()
        {
            return isMobile;
        }

        public Animator GetAnimator()
        {
            return animator;
        }

        private void AddRequiredComponents()
        {          
            rigidBody = gameObject.AddComponent<Rigidbody>();
            if (isMobile)
            {
                rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                rigidBody.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            }

            rigidBody.mass = mass;

            audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.spatialBlend = audioSourceSpatialBlend;
            audioSource.volume = 0.6f;

            animator = gameObject.AddComponent<Animator>();
            animator.runtimeAnimatorController = animatorController;
            animator.avatar = characterAvatar;
        }

        public AnimatorOverrideController GetOverrideController()
        {
            return animatorOverrideController;
        }

        public float GetAnimationSpeedMultiplier()
        {
            return animationSpeedMultiplier;
        }

        void OnAnimatorMove()
        {
            // we implement this function to override the default root motion.
            // this allows us to modify the positional speed before it's applied.
            if (Time.deltaTime > 0)
            {
                Vector3 v = (animator.deltaPosition * moveSpeedMultiplier) / Time.deltaTime;

                // we preserve the existing y part of the current velocity.
                v.y = rigidBody.velocity.y;
                rigidBody.velocity = v;
            }            
        }

        public void Move(Vector3 move)
        {
            SetForwardAndTurn(move);

            ApplyExtraTurnRotation();

            // send input and other state parameters to the animator
            UpdateAnimator();
        }

        private void SetForwardAndTurn(Vector3 move)
        {
            // convert the world relative moveInput vector into a local-relative
            // turn amount and forward amount required to head in the desired
            // direction.
            if (move.magnitude > moveThreshold) move.Normalize();
            Vector3 localMove = transform.InverseTransformDirection(move);
            turnAmount = Mathf.Atan2(localMove.x, localMove.z);
            forwardAmount = localMove.z;
        }



        void UpdateAnimator()
        {
            // update the animator parameters
            animator.SetFloat("Forward", forwardAmount, 0.1f, Time.deltaTime);
            animator.SetFloat("Turn", turnAmount, 0.1f, Time.deltaTime);
            animator.speed = animationSpeedMultiplier;
        }

        void ApplyExtraTurnRotation()
        {
            // help the character turn faster (this is in addition to root rotation in the animation)
            float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);
            transform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
        }

        public void Kill()
        {
            isAlive = false;
        }

        void OnCollisionStay(Collision col)
        {
            if (col.gameObject.tag == "Enemy")
            {
                
            }
        }

        void OnCollisionExit(Collision col)
        {
            if (col.gameObject.tag == "Enemy")
            {

            }
        }

        //Receivers for animation events
        private void Step()
        {
            PlayFootStepSound();
        }

        void FootR()
        {
            PlayFootStepSound();
        }

        void FootL()
        {
            PlayFootStepSound();
        }

        private void PlayFootStepSound()
        {
            if (footStepSound != null)
            {
                audioSource.PlayOneShot(footStepSound);
            }
        }
    }
}