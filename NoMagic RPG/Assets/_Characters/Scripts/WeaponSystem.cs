﻿using RPG.Character;
using RPG.Game;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace RPG.Characters
{
    public class WeaponSystem : MonoBehaviour
    {
        private const string ATTACK_TRIGGER = "Attack";
        private const string SHOOT_TRIGGER = "Shoot";
        private const string DEFAULT_ATTACK = "DEFAULT ATTACK";

        GameObject target;

        [SerializeField]
        float baseDamage = 20f;

        [SerializeField]
        WeaponConfig weaponInUse;

        Character character;
        GameObject weaponObject;
        Animator animator;
        float lastHitTime;

        // Use this for initialization
        void Start()
        {
            animator = GetComponent<Animator>();
            character = GetComponent<Character>();

            if (character.GetIsMobile() && weaponInUse != null)
            {
                PutWeaponInHand(weaponInUse);

                SetupAttackAnimation();
            }

            GameManager.instance.notifyWeaponObservers += OnPlayerWeaponChange;
        }

        private void OnPlayerWeaponChange(int weaponIndex)
        {
            if (gameObject.tag.Equals("Player"))
            {
                var weapon = Resources.Load<WeaponConfig>(GameManager.instance.weapons[weaponIndex].path);
                GameManager.instance.selectedWeapon = weaponIndex;
                if (character.GetIsMobile())
                    PutWeaponInHand(weapon);
            }

        }

        // Update is called once per frame
        void Update()
        {
            bool targetIsDead, targetIsOutOfRange;
            if (target == null)
            {
                targetIsDead = false;
                targetIsOutOfRange = false;
            }
            else
            {
                float distanceToTarget = 0.0f;
                if (target.GetComponent<Character>().GetIsMobile())
                    distanceToTarget = Vector3.Distance(transform.position, target.transform.position);
                else
                    distanceToTarget = Vector3.Distance(transform.position, target.GetComponentInChildren<Weakpoint>().transform.position);
                float targetHealth = target.GetComponent<HealthSystem>().healthAsPercentage();

                targetIsDead = targetHealth <= Mathf.Epsilon;
                targetIsOutOfRange = distanceToTarget > GetCurrentWeapon().GetMaxAttackRange();
            }

            float characterHealth = GetComponent<HealthSystem>().healthAsPercentage();
            bool characterIsDead = (characterHealth <= Mathf.Epsilon);

            if (characterIsDead || targetIsOutOfRange || targetIsDead)
            {
                StopAllCoroutines();
            }
        }

        public void StopAttacking()
        {
            animator.StopPlayback();
            StopAllCoroutines();
        }

        public void AttackTarget(GameObject targetToAttack)
        {
            target = targetToAttack;
            StartCoroutine(AttackTarget());
        }

        IEnumerator AttackTarget()
        {
            //determine if still alive (attacker and defender)
            bool attackerStillAlive = GetComponent<HealthSystem>().healthAsPercentage() >= Mathf.Epsilon;
            bool targetStillAlive = target.GetComponent<HealthSystem>().healthAsPercentage() >= Mathf.Epsilon;

            if (attackerStillAlive && targetStillAlive)
            {

                var animationClip = GetCurrentWeapon().GetAnimClip();
                float animationClipTime = animationClip.length / character.GetAnimationSpeedMultiplier();
                float timeToWait = animationClipTime + GetCurrentWeapon().GetMinTimeBetweenAnimationCycles();

                bool isTimeToHitAgain = Time.time - lastHitTime > timeToWait;

                if (isTimeToHitAgain)
                {
                    Attack();
                    lastHitTime = Time.time;
                }

                yield return new WaitForSeconds(timeToWait);
            }
        }

        private void Attack()
        {

            if (character.GetIsMobile())
            {
                if (target.GetComponent<WeaponSystem>().character.GetIsMobile())
                    transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z), Vector3.up); //target position y has to be the attacker position y to prevent vertical rotations.
                else
                {
                    transform.LookAt(new Vector3(target.GetComponentInChildren<Weakpoint>().transform.position.x, transform.position.y, target.GetComponentInChildren<Weakpoint>().transform.position.z), //target position y has to be the attacker position y to prevent vertical rotations.
                    Vector3.up);

                }

                animator.SetTrigger(ATTACK_TRIGGER);
            }
            else
            {
                animator.SetTrigger(SHOOT_TRIGGER);
            }
            //float damageDelay = 1.0f; //todo get from weapon
            SetupAttackAnimation();
            if (character.GetIsMobile())
            {
                target.GetComponent<HealthSystem>().TakeDamage(CalculateDamage());
            }
            else
            {
                Shoot(weaponInUse);
            }
            //StartCoroutine(DamageAfterDelay(damageDelay));
        }

        IEnumerator DamageAfterDelay(float damageDelay)
        {
            yield return new WaitForSeconds(damageDelay);
            target.GetComponent<HealthSystem>().TakeDamage(CalculateDamage());
        }

        public WeaponConfig GetCurrentWeapon()
        {
            return weaponInUse;
        }

        private void SetupAttackAnimation()
        {
            if (!character.GetOverrideController())
            {
                Debug.Break();
                Debug.LogAssertion("Please provide " + gameObject + " with an animator override controller.");
            }
            else
            {
                var animatorOverrideController = character.GetOverrideController();
                animator.runtimeAnimatorController = animatorOverrideController;
                animatorOverrideController[DEFAULT_ATTACK] = weaponInUse.GetAnimClip();
            }
        }

        public void PutWeaponInHand(WeaponConfig weaponToUse)
        {
            weaponInUse = weaponToUse;
            var weaponPrefab = weaponInUse.GetWeaponPrefab();

            GameObject dominantHand = RequestDominantHand();
            Destroy(weaponObject);
            weaponObject = Instantiate(weaponPrefab, dominantHand.transform);

            //Stop animation of loot weapon
            if (weaponObject.GetComponent<Animator>())
                weaponObject.GetComponent<Animator>().SetBool("Stop", true);

            weaponObject.transform.localPosition = weaponInUse.gripTransform.localPosition;
            weaponObject.transform.localRotation = weaponInUse.gripTransform.localRotation;
            weaponObject.transform.localScale = weaponInUse.gripTransform.localScale;

            if (gameObject.CompareTag("Player"))
                GameManager.instance.selectedWeapon = weaponInUse.GetWeaponId();

        }

        public void TakeWeaponInHand()
        {
            weaponInUse = null;
            Destroy(weaponObject);
            if (gameObject.tag.Equals("Player"))
            {
                //Check if player has at least one weapon and return them to the player
                if (GameManager.instance.weapons.Count >= 1)
                {
                    foreach (var weapon in GameManager.instance.weapons)
                    {
                        GameManager.instance.OnPlayerWeaponOnChange(weapon.Key);
                        break;
                    }
                }
                else
                {
                    GameManager.instance.selectedWeapon = 0;
                }


            }


        }

        private void Shoot(WeaponConfig weaponToUse)
        {
            weaponInUse = weaponToUse;
            GameObject shootHole = RequestShootPosition();

            GameObject projectile = Instantiate(weaponInUse.GetWeaponBullet(), shootHole.transform);


            projectile.transform.localPosition = weaponInUse.gripTransform.localPosition;
            projectile.transform.localRotation = weaponInUse.gripTransform.localRotation;
            projectile.GetComponent<Projectile>().SetDamage(CalculateDamage());
            projectile.GetComponent<Projectile>().SetShooter(gameObject);


            Vector3 unitVectorToTarget = (target.transform.position - transform.position).normalized;
            projectile.GetComponent<Rigidbody>().velocity = unitVectorToTarget * projectile.GetComponent<Projectile>().GetDefaultProjectileSpeed();

        }

        private GameObject RequestShootPosition()
        {
            var shootHoles = GetComponentsInChildren<ShootHole>();
            int numberOfShootHoles = shootHoles.Length;
            Assert.IsFalse(numberOfShootHoles <= 0, "No ShootHole found on " + gameObject.name + ", please add one.");
            Assert.IsFalse(numberOfShootHoles > 1, "Multiple ShootHole on " + gameObject.name + ", please remove one.");
            return shootHoles[0].gameObject;
        }


        private GameObject RequestDominantHand()
        {
            var dominantHands = GetComponentsInChildren<DominantHand>();
            int numberOfDominantHands = dominantHands.Length;
            Assert.IsFalse(numberOfDominantHands <= 0, "No DominantHand found on " + gameObject.name + ", please add one.");
            Assert.IsFalse(numberOfDominantHands > 1, "Multiple DominantHand on " + gameObject.name + ", please remove one.");
            return dominantHands[0].gameObject;
        }

        private float CalculateDamage()
        {
            return baseDamage + weaponInUse.GetExtraDamage();
        }

    }
}
