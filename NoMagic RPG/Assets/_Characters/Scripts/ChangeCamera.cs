﻿using RPG.Cameras;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Characters
{
    public class ChangeCamera : MonoBehaviour
    {
        [SerializeField]
        Transform target;

        [SerializeField]
        bool isCameraRotationLocked;

        // Start is called before the first frame update
        void Start()
        {

        }

        private void ToggleCameraTarget()
        {
            FindObjectOfType<FreeLookCam>().SetTarget(target);
            FindObjectOfType<FreeLookCam>().SetLockCameraRotation(isCameraRotationLocked);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
