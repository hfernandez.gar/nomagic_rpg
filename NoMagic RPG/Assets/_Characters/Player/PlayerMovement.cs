using UnityEngine;
using RPG.Cameras;
using System;
using RPG.Characters.Enemy;
using RPG.Characters.NPC;
using RPG.Characters;
using RPG.Game;
using RPG.Menu;
using VIDE_Data;
using RPG.UI;

namespace RPG.Characters.Player
{
    public class PlayerMovement : MonoBehaviour
    {

        [SerializeField]
        float interactRadius = 0.2f;
        [SerializeField]
        const int enemyLayerNumber = 9;
        [SerializeField]
        const int npcLayerNumber = 10;

        InputRaycaster raycaster;
        Rigidbody rigidBody;

        public static bool isInJoystickMode = false;

        //Store character movement
        private float h, v;


        private Material oldMat;
        private GameObject currentEnemy;
        private Animator animator;
        private Character character;

        public float GetInteractRadius()
        {
            return interactRadius;
        }

        private void Awake()
        {
            character = GetComponent<Character>();
        }

        private void Start()
        {
           
            raycaster = Camera.main.GetComponent<InputRaycaster>();
            raycaster.notifyEnemyObservers += OnInputEnemy;
            raycaster.notifyNPCObservers += OnInputNPC;
            raycaster.notifyDestroyableObservers += OnInputDestroyable; 

        }

        private void OnInputDestroyable(DestroyThingByPlayer destroyable)
        {
            if (VD.isActive) { return; }
        }

        private void OnInputNPC(NPC.NPC npc)
        {
            if(VD.isActive) { return; }

            if (PlayerMovement.isInJoystickMode)
            {
                if (Input.GetButtonDown("JoystickButtonA"))
                {
                   
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                   
                }
            }
        }

        private void OnInputEnemy(EnemyAI enemy)
        {
            if(VD.isActive) { return; }

            float distanceTo = 0;
            if (PlayerMovement.isInJoystickMode)
            {
                if (Input.GetButtonDown("JoystickButtonA"))
                {
                    distanceTo = Vector3.Distance(PlayerManager.instance.playerTransform.position, enemy.gameObject.transform.position);
                    if (distanceTo <= interactRadius)
                    {
                        Debug.Log("Fighting enemy");
                    }
                }
            }
            else
            {
                if (Input.GetMouseButton(0))
                {
                    distanceTo = Vector3.Distance(PlayerManager.instance.playerTransform.position, enemy.gameObject.transform.position);
                    if (distanceTo <= interactRadius)
                    {
                        Debug.Log("Fighting enemy");
                    }
                }
            }
        }

        protected void OnEnable()
        {
            GameInputManager.ObserveKeyCode(KeyCode.G);
            GameInputManager.ObserveKeyCode(KeyCode.Alpha1);
            GameInputManager.ObserveKeyCode(KeyCode.Alpha2);
            GameInputManager.ObserveKeyCode(KeyCode.B);

            GameInputManager.ObserveAxis("HorizontalJoystick");
            GameInputManager.ObserveAxis("VerticalJoystick");

            GameInputManager.ObserveAxis("VerticalKeyboard");
            GameInputManager.ObserveAxis("HorizontalKeyboard");

            GameInputManager.ObserveButton("JoystickButtonA");
            GameInputManager.ObserveButton("JoystickButtonB");
            GameInputManager.ObserveButton("MouseButtonA");
            GameInputManager.ObserveButton("MouseButtonB");

            GameInputManager.ObserveButton("JoystickButtonR");
            GameInputManager.ObserveButton("JoystickButtonL");



            GameInputManager.Register(OnInputEvent);
        }

        protected void OnDisable()
        {
            GameInputManager.Unregister(OnInputEvent);
            raycaster.notifyNPCObservers -= OnInputNPC;
            raycaster.notifyEnemyObservers -= OnInputEnemy;
        }

        protected void OnInputEvent(GameInputManager.EventData data)
        {

            if (VD.isActive)
            {
                if(!VD.nodeData.pausedAction && VD.nodeData.isPlayer)
                {
                    if (isInJoystickMode)
                    {
                        if (Input.GetJoystickNames().Length > 0)
                        {

                            if (data.axis == "VerticalJoystick")
                            {
                                
                                if(data.value > 0)
                                {
                                    if(VD.nodeData.commentIndex < UIManager.playerOptionsLength)
                                        VD.nodeData.commentIndex++;
                                }
                                else if(data.value < 0)
                                {
                                    if (VD.nodeData.commentIndex > 0)
                                        VD.nodeData.commentIndex--;
                                }
                            }

                        }
                    }
                   

                }
                return;
            }

            if (data.used) return;


            switch (data.keyCode)
            {
                //FOR USE AS TEST MODE
                /*case KeyCode.G:
                    isInJoystickMode = !isInJoystickMode; // toggle mode
                    Cursor.visible = !Cursor.visible; //Make the cursor invisible or visible
                    Cursor.lockState = (Cursor.visible ? CursorLockMode.None : CursorLockMode.Locked);
                    break;*/

                case KeyCode.P:
                    var hud = FindObjectOfType<HUD>();
                    var pauseMenu = FindObjectOfType<PauseMenu>();
                    var weaponsMenu = FindObjectOfType<WeaponsMenu>();
                    var gameState = TimeUtils.timeScaleToggle();
                    switch (gameState)
                    {
                        case TimeUtils.GameState.Paused:
                            hud.GetComponent<CanvasGroup>().alpha = 0;


                            pauseMenu.gameObject.GetComponent<CanvasGroup>().alpha = 1.0f;
                            pauseMenu.gameObject.GetComponent<CanvasGroup>().interactable = true;
                            pauseMenu.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;

                            break;

                        case TimeUtils.GameState.Resume:
                            hud.GetComponent<CanvasGroup>().alpha = 1.0f;


                            pauseMenu.gameObject.GetComponent<CanvasGroup>().alpha = 0f;
                            pauseMenu.gameObject.GetComponent<CanvasGroup>().interactable = false;
                            pauseMenu.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;

                            weaponsMenu.GetComponent<CanvasGroup>().alpha = 0;
                            weaponsMenu.GetComponent<CanvasGroup>().interactable = false;
                            weaponsMenu.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;

                            break;
                    }

                    break;
            }

            if (isInJoystickMode)
            {
                if (Input.GetJoystickNames().Length > 0)
                {
                    if (data.axis == "HorizontalJoystick")
                    {
                        h = data.value;
                    }

                    if (data.axis == "VerticalJoystick")
                    {
                        v = data.value;
                    }

                }
            }
            else
            {
                if (data.axis == "HorizontalKeyboard")
                {
                    h = data.value;
                }

                if (data.axis == "VerticalKeyboard")
                {
                    v = data.value;
                }
            }
        }

        public void Kill()
        {
            //kill signaling
            character.GetAnimator().SetFloat("Forward", 0.0f);
            character.GetAnimator().SetFloat("Turn", 0.0f);
            StopAllCoroutines();
        }

   
        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            if (VD.isActive || Camera.main == null) { return; }
            PlayerManager.instance.playerTransform = transform;
            ProccessMovement();
        }

        private void ProccessMovement()
        {
            float h = this.h;
            float v = this.v;
            PlayerManager.instance.h = this.h;
            PlayerManager.instance.v = this.v;

            // calculate camera relative direction to move:
            Vector3 cameraForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
            Vector3 movement = v * cameraForward + h * Camera.main.transform.right;

            character.Move(movement);

            PlayerManager.instance.playerTransform = transform;
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.black;
            Gizmos.DrawWireSphere(transform.position, interactRadius);
            //Draw attack sphere
            if (currentEnemy != null)
            {
                Gizmos.DrawLine(transform.position, currentEnemy.transform.position);
            }
        }

        
    }
}

