﻿using UnityEngine;
using UnityEngine.Assertions;
using RPG.Utilities;
using RPG.Cameras;
using System;
using RPG.Characters;
using System.Collections;
using UnityEngine.SceneManagement;
using VIDE_Data;
using System.Collections.Generic;
using RPG.UI;
using RPG.Game;

namespace RPG.Characters.Player
{
    public class Player : MonoBehaviour
    {

        Character character;
        SpecialAbilities specialAbilities;
        WeaponSystem weaponSystem;
        PlayerMovement playerMovement;

        InputRaycaster raycaster;
        GameObject currentTarget;
        Enemy.EnemyAI currentEnemy = null;
        int abilityIndex = 0;

        //Lets grab the NPC's VIDE_Assign script, if there's any
        VIDE_Assign assigned;


        public int GetAbilityIndex()
        {
            return abilityIndex;
        }

        public void AddSpecialAbility(SpecialAbilityConfig sa)
        {
            this.specialAbilities.AddSpecialAbility(sa);
        }

        public List<Sprite> GetSpritesFromSpecialAbilities()
        {
            return specialAbilities.GetAbilitiesSprites();
        }

        void Start()
        {
            RegisterForInput();

            character = GetComponent<Character>();
            specialAbilities = GetComponent<SpecialAbilities>();
            weaponSystem = GetComponent<WeaponSystem>();
            playerMovement = GetComponent<PlayerMovement>();       
        }

        void Update()
        {
            float healthPercentate = GetComponent<HealthSystem>().healthAsPercentage();
            if(healthPercentate <= Mathf.Epsilon)
            {
                GameInputManager.Unregister(OnInputEvent);
                return;
            }
        }

        protected void OnEnable()
        {
            GameInputManager.Register(OnInputEvent);
        }

        protected void OnInputEvent(GameInputManager.EventData data)
        {
            if (data.used) return;
            if (VD.isActive)
            {
                ReadDialogue(data);
            }
            else
            {
                ChangeSpecialAbility(data);
            }
            
           
        }

        private void ReadDialogue(GameInputManager.EventData data)
        {
            switch (data.button)
            {
                case "MouseButtonA":
                    if (!PlayerMovement.isInJoystickMode) { ContinueDialogue(); };
                    break;
                case "JoystickButtonA":
                    if (PlayerMovement.isInJoystickMode) { ContinueDialogue(); };
                    break;
            }
        }

        private void ChangeSpecialAbility(GameInputManager.EventData data)
        {
            int i = abilityIndex;

            if (!PlayerMovement.isInJoystickMode)
            {
                switch (data.keyCode)
                {
                    case KeyCode.Alpha1:
                        abilityIndex -= 1;
                        break;
                    case KeyCode.Alpha2:
                        abilityIndex += 1;
                        break;

                }

                switch (data.button)
                {
                    case "MouseButtonB":
                        //Use healing
                        if (specialAbilities.isAbilityIndexSelfHealAbility(i))
                            specialAbilities.AttemptSpecialAbility(abilityIndex, gameObject);
                        break;
                }
            }
            else
            {
                switch (data.button)
                {
                    case "JoystickButtonL":
                        abilityIndex -= 1;
                        break;
                    case "JoystickButtonR":
                        abilityIndex += 1;
                        break;
                    case "JoystickButtonB":
                        //Use healing
                        if (specialAbilities.isAbilityIndexSelfHealAbility(i))
                            specialAbilities.AttemptSpecialAbility(abilityIndex, gameObject);
                        break;
                }
            }

            i = abilityIndex;

            //Loop between the special abilities
            if (i < 0)
                i = specialAbilities.GetNumberAbilities() - 1;
            else if (i > specialAbilities.GetNumberAbilities() - 1)
                i = 0;
            
            abilityIndex = i;
            
        }

        private void RegisterForInput()
        {
            raycaster = Camera.main.GetComponent<InputRaycaster>();
            raycaster.notifyEnemyObservers += OnInputEnemy;
            raycaster.notifyNPCObservers += OnInputNPC;
            raycaster.notifyDestroyableObservers += OnInputDestroyable;
        }

        private bool IsPlayerNotAbleToAttack()
        {
            return (VD.isActive || weaponSystem.GetCurrentWeapon() == null);
        }

        private void OnInputDestroyable(DestroyThingByPlayer destroyable)
        {
            if (IsPlayerNotAbleToAttack()) { return; }
            if (PlayerMovement.isInJoystickMode)
            {
                if (Input.GetButtonDown("JoystickButtonB"))
                {
                    if (IsTargetInRange(destroyable.gameObject,true, abilityIndex))
                    {
                        if (!specialAbilities.isAbilityIndexSelfHealAbility(abilityIndex))
                            specialAbilities.AttemptSpecialAbility(abilityIndex, destroyable.gameObject);
                    }
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(1))
                {
                    if (IsTargetInRange(destroyable.gameObject, true, abilityIndex))
                    {
                        if (!specialAbilities.isAbilityIndexSelfHealAbility(abilityIndex))
                            specialAbilities.AttemptSpecialAbility(abilityIndex, destroyable.gameObject);
                    }
                }                    
            }
        }

        private void OnInputNPC(NPC.NPC npc)
        {
            if (IsPlayerNotAbleToAttack()) { return; }

            if (PlayerMovement.isInJoystickMode)
            {
                if (Input.GetButtonDown("JoystickButtonA"))
                {
                    if (IsTargetInRange(npc.gameObject))
                        TryDialogue(npc.gameObject);
                }
            }
            else
            {
                if (Input.GetMouseButton(0))
                    if (IsTargetInRange(npc.gameObject))
                        TryDialogue(npc.gameObject);
            }
        }

        private void OnInputEnemy(Enemy.EnemyAI enemy)
        {
            if (IsPlayerNotAbleToAttack()) { return; }

            this.currentEnemy = enemy;
            if (PlayerMovement.isInJoystickMode)
            {
                if (Input.GetButtonDown("JoystickButtonA"))
                {
                    if (IsTargetInRange(currentEnemy.gameObject))
                    {
                        weaponSystem.AttackTarget(currentEnemy.gameObject);
                    }
                }
                else if (Input.GetButtonDown("JoystickButtonB"))
                {
                    if (!specialAbilities.isAbilityIndexSelfHealAbility(abilityIndex))
                        specialAbilities.AttemptSpecialAbility(abilityIndex, currentEnemy.gameObject);
                }

            }
            else
            {
                if (Input.GetMouseButton(0))
                {
                    if (IsTargetInRange(currentEnemy.gameObject))
                    {
                        weaponSystem.AttackTarget(currentEnemy.gameObject);
                    }
                }
                else if (Input.GetMouseButtonDown(1))
                {
                    if (!specialAbilities.isAbilityIndexSelfHealAbility(abilityIndex))
                        specialAbilities.AttemptSpecialAbility(abilityIndex, currentEnemy.gameObject);
                }
            }
        }
        
        private bool IsTargetInRange(GameObject target, bool isSpecialAttack = false, int specialAttackIndex = 0)
        {
            //Check target is in range
            float distanceToTarget = 0.0f;
            float maxAttackRange = 0.0f;
            if(target.GetComponent<Character>().GetIsMobile() != null)
                distanceToTarget = (target.transform.position - transform.position).magnitude;
            else if(target.GetComponent<DestroyThingByPlayer>() != null)
            {
                distanceToTarget = (target.GetComponentInParent<GameObject>().transform.position - transform.position).magnitude;
            }
            else if(!target.GetComponent<Character>().GetIsMobile())
                distanceToTarget = (target.GetComponentInChildren<Weakpoint>().transform.position - transform.position).magnitude;

            if (isSpecialAttack && (this.specialAbilities.GetSpecialAbilityByIndex(specialAttackIndex) as AreaOfEffectConfig))
            {
                maxAttackRange = (this.specialAbilities.GetSpecialAbilityByIndex(specialAttackIndex) as AreaOfEffectConfig).GetRadius();
            }
            else
            {
                maxAttackRange = weaponSystem.GetCurrentWeapon().GetMaxAttackRange();
            }
            return distanceToTarget <= maxAttackRange;
        }

        private void OnDisable()
        {
            raycaster.notifyEnemyObservers -= OnInputEnemy;
            raycaster.notifyNPCObservers -= OnInputNPC;
        }

  

        //Casts a ray to see if we hit an NPC and, if so, we interact
        void TryDialogue(GameObject npc)
        {            
            assigned = npc.GetComponent<VIDE_Assign>();
               
            UIManager.instance.Interact(assigned); //Begins interaction

            playerMovement.Kill();

            transform.LookAt(new Vector3(npc.transform.position.x, transform.position.y, npc.transform.position.z), Vector3.up);
        }

        void ContinueDialogue()
        {
            UIManager.instance.Interact(assigned);
        }
    }
}
