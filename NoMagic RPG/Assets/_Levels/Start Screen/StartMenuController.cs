﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace RPG.Game
{
    public class StartMenuController : MonoBehaviour
    {
        //Add more elements when needed

        [SerializeField]
        private Button startButton;

        // Start is called before the first frame update
        void Start()
        {
            startButton = GetComponent<Button>();
        }


        public void OnStartButtonClick()
        {
            SceneManager.LoadScene("PrototypeLevel");
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}